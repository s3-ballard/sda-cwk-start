/*
  ==============================================================================

    FilePlayer.cpp
    Created: 22 Jan 2013 2:49:14pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayer.h"


FilePlayer::FilePlayer() : thread("FilePlayThread"), resamplingAudioSource(&audioTransportSource, false)
{
    thread.startThread();
    currentAudioFileSource = NULL;
    newTime = 0;
    //resamplingAudioSource = new ResamplingAudioSource(&audioTransportSource, false);
}

/**
 Destructor
 */
FilePlayer::~FilePlayer()
{
    audioTransportSource.setSource(0);//unload the current file
    deleteAndZero(currentAudioFileSource);//delete the current file
    
    thread.stopThread(100);
    //delete resamplingAudioSource;
}

/**
 Starts or stops playback of the looper
 */
void FilePlayer::setPlaying (const bool newState)
{
    if(newState == true)
    {
        audioTransportSource.setPosition(newTime);
        audioTransportSource.start();
    }
    else
    {
        audioTransportSource.stop();
    }
}

/**
 Gets the current playback state of the looper
 */
bool FilePlayer::isPlaying () const
{
    return audioTransportSource.isPlaying();
}


/**
 Loads the specified file into the transport source
 */
void FilePlayer::loadFile(const File& newFile)
{
    // unload the previous file source and delete it..
    setPlaying(false);
    audioTransportSource.setSource (0);
    deleteAndZero (currentAudioFileSource);
    
    // create a new file source from the file..
    // get a format manager and set it up with the basic types (wav, ogg and aiff).
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    AudioFormatReader* reader = formatManager.createReaderFor (newFile);
    
    if (reader != 0)
    {
        //currentFile = audioFile;
        currentAudioFileSource = new AudioFormatReaderSource (reader, true);
        
        // ..and plug it into our transport source
        audioTransportSource.setSource (currentAudioFileSource,
                                   32768, // tells it to buffer this many samples ahead
                                   &thread,
                                   reader->sampleRate);
    }
}

//AudioSource
void FilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    //audioTransportSource.prepareToPlay (samplesPerBlockExpected, sampleRate);
    resamplingAudioSource.prepareToPlay (samplesPerBlockExpected, sampleRate);
}

void FilePlayer::releaseResources()
{
    audioTransportSource.releaseResources();

    //resamplingAudioSource->releaseResources();
    
}

void FilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    //audioTransportSource.getNextAudioBlock (bufferToFill);
    resamplingAudioSource.getNextAudioBlock(bufferToFill);
    
    //do this for the loop part, other wise use a counter
    //if (audioTransportSource.getCurrentPosition() == audioTransportSource.getLengthInSeconds();
}

void FilePlayer::setPosition(float newPosition)
{
    newTime = audioTransportSource.getLengthInSeconds();
    newTime *= newPosition;
    audioTransportSource.setPosition(newTime);
}

float FilePlayer::getPosition()
{
    float pos = audioTransportSource.getCurrentPosition();
    pos = pos / audioTransportSource.getLengthInSeconds();
    return pos;
}

void FilePlayer::setPlaybackRate(float newRate)
{
    resamplingAudioSource.setResamplingRatio(newRate);
    playbackRate = newRate;
}


void FilePlayer::randomiseParam(float originalVal)
{
    //need to call at initialization and whenever a new seed button is entered.
    //will need to change this value to something more appropriate
    randValue = (rand() % 2) * originalVal;
    randValue += 0.1;
}

void FilePlayer::generativeAmount(float amountSliderVal)
{
    //for the playback rate;
    float randomVal = randValue; //may be able to delete
    //morph between the original and the randomized amount using the slider
    float genPlayRate = ((1.f - amountSliderVal) * playbackRate) + (amountSliderVal * randomVal);

    //may get rid of resample ratio if using BPM or try to trigger a new resample ratio at every BPM hit
    resamplingAudioSource.setResamplingRatio(genPlayRate);
    
}
/* may need to add back in after some changing
void FilePlayer::setLoop()
{
    //when the loop is set to true then 
    if (Loop == true)
    {
        if (audioTransportSource.getCurrentPosition() == audioTransportSource.getTotalLength())
        {
        audioTransportSource.setPosition(newTime);
        audioTransportSource.start();
        }
    }
    else
    {
        audioTransportSource.stop();
    }
    
}
 */

void FilePlayer::TempoChange(float tempo, int sampRate)
{
    
    bool tempoOn = true; //DELETE
    int sR = sampRate; //get sample rate from the main component
    float sampMs;
    //get bpm in samples.
    float tempTime = 60000 / tempo;
    tempTime = tempTime / sampRate;
    
    //get audio file in samples
    sampMs = (audioTransportSource.getLengthInSeconds() * 1000) / sR;
    
    if (tempoOn == true)
    {
        //get the appropriate ratio of samples between the tempo and the audio file
        ratio = sampMs / tempTime;
        ratio *= 0.25; //may have to change for different beat divisions
        resamplingAudioSource.setResamplingRatio(ratio); //set the new ratio
        
    }
    //probably delete
    else if (tempoOn == false)
    {
        
    }
    
    
    
    
    
    
    
}
