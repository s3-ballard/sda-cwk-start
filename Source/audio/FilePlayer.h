/*
  ==============================================================================

    FilePlayer.h
    Created: 22 Jan 2013 2:49:14pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYER
#define H_FILEPLAYER

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Simple FilePlayer class - strams audio from a file.
 */
class FilePlayer : public AudioSource
{
public:
    /**
     Constructor
     */
    FilePlayer();
    
    /**
     Destructor
     */
    ~FilePlayer();
    
    /**
     Starts or stops playback of the looper
     */
    void setPlaying (const bool newState);
    
    /**
     Gets the current playback state of the looper
     */
    bool isPlaying () const;
    
    /**
     Loads the specified file into the transport source
     */
    void loadFile(const File& newFile);
    
    //AudioSource
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate);
    void releaseResources();
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
    
    void setPosition (float newPosition);
    
    float getPosition();
    
    void setPlaybackRate (float newRate);
    
    //new stuff
    void randomiseParam (float originalVal);  //randomise a parameter with a new seed
    void generativeAmount(float amountSliderVal); //morph between normal and random amount
    bool Loop;
    
    void TempoChange(float tempo, int sampRate); //adjust the resample ratio to a desired Tempo
private:
    AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;	// this controls the playback of a positionable audio stream, handling the
                                            // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                 //thread for the transport source
    float newTime;
    float playbackRate;
    float randValue;
    float ratio;
    
    ResamplingAudioSource resamplingAudioSource;
};

#endif  // H_FILEPLAYER
