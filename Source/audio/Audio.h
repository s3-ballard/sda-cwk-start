/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
 
    
    
    
    FilePlayer& getFilePlayer(const int index) { return filePlayer[index]; }
    
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;

    void audioDeviceStopped() override;
    
    Atomic<float> gain;
    Atomic<float> sampleRate;
    
    //floats for the amplitude of the four corners of the grid
    Atomic<float> xyAmpTL;
    Atomic<float> xyAmpTR;
    Atomic<float> xyAmpBL;
    Atomic<float> xyAmpBR;
    
    //Atomic<float> xGain;
    //Atomic<float> yGain; - use these two so set xGain as audio.xgain = XYgrid.x.getValue()
private:
    AudioDeviceManager audioDeviceManager;
    AudioSourcePlayer audioSourcePlayer;
    
    
    static const int numFiles = 2;
    FilePlayer filePlayer[numFiles];
    MixerAudioSource mixer;
    
    
};



#endif  // AUDIO_H_INCLUDED
