/*
 ==============================================================================
 
 FilePlayerGui.cpp
 Created: 22 Jan 2013 2:49:07pm
 Author:  tj3-mitchell
 
 ==============================================================================
 */

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    //NEED TO CHANGE NAME - actually position
    
    timeSlider.addListener(this);
    addAndMakeVisible(&timeSlider);
    timeSlider.setRange(0.0, 1.0);
    
    //controls overall speed
    pitchSlider.setRange(0.01, 5.0);
    addAndMakeVisible(&pitchSlider);
    pitchSlider.addListener(this);
    pitchSlider.setValue(1.0);
    
    //generative control - maybe remove
    randomise.setButtonText("randomise");
    randomise.addListener(this);
    addAndMakeVisible(&randomise);
    
    randomSlider.setRange(0.0, 1.0);
    addAndMakeVisible(&randomSlider);
    randomSlider.setValue(0.0);
    randomSlider.addListener(this);
    
    loopButton.setButtonText("loop");
    loopButton.addListener(this);
    addAndMakeVisible(&loopButton);
    
    TempSync.setButtonText("tempo Sync");
    TempSync.addListener(this);
    addAndMakeVisible(&TempSync);
    
    //initialise both tempo lock and loop control to off
    tempoOnOrOff = false;
    filePlayer.Loop = false;
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    //will need to change all of the sizes as a bit overkill at the moment
    playButton.setBounds (0, 0, getWidth() * 0.5, 50);
    fileChooser->setBounds (getWidth()- (getWidth() * 0.5), 0, getWidth() * 0.5, 50);
    timeSlider.setBounds(0, 50, getWidth(), 100);
    pitchSlider.setBounds(0, 150, getWidth(), 100);
    randomise.setBounds(0, 250, getWidth(), 50);
    randomSlider.setBounds(0, 300, getWidth(), 100);
    loopButton.setBounds(0, 400, getWidth(), 50);
    TempSync.setBounds(0, 450, getWidth(), 50);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        //start the playback
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true){
            startTimer(250);
        }
        else if (filePlayer.isPlaying() == false){
            stopTimer();
            //when the play button is turned off then loop will turn off - LINK LOOP AND SINGLE TOGETHER as a switch.
            filePlayer.Loop = false;
        }
    }
    
    //randomise parameters - probably change or remove
    if (button== &randomise){
        //set random with the current pitch slider value as the seed
        filePlayer.randomiseParam(pitchSlider.getValue());
    }
    //need to neaten
    if (button == &loopButton)
    {
        //switch between loop states
        if (filePlayer.Loop == false)
            filePlayer.Loop = true;
        else if (filePlayer.Loop == true)
            filePlayer.Loop = false;
        //need to find a way that turns this off only at the end of the loop
        filePlayer.setPlaying(filePlayer.Loop);
        
    }
    if (button == &TempSync)
    {
        //Switch between tempo sync states
        if (tempoOnOrOff == false)
            tempoOnOrOff = true;
        else if (tempoOnOrOff == true)
            tempoOnOrOff = false;
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    //position slider - probably change to become the start of a loop point
    if (slider == &timeSlider)
        filePlayer.setPosition(timeSlider.getValue());
    if (slider == &pitchSlider)
        filePlayer.setPlaybackRate(pitchSlider.getValue());
    //morph between normal and randomized param - look at doing loop points
    if (slider == &randomSlider)
        filePlayer.generativeAmount(randomSlider.getValue());
}

void FilePlayerGui::timerCallback()
{
    //automated position slider - may remove
    timeSlider.setValue(filePlayer.getPosition());
    
    if (filePlayer.Loop == true)
    {
        //repeat the file once it has ended
        if (filePlayer.isPlaying() == false){
            filePlayer.setPosition(0);
            filePlayer.setPlaying(true);
        }
    }
}
//send desired BPM (need sample rate for equations
void FilePlayerGui::tempoSync(float BPM, int sampleRate)
{
    if (tempoOnOrOff == true){
        filePlayer.TempoChange(BPM, sampleRate);
    }
    else if (tempoOnOrOff == false){
        //just skip PROB DELETE THIS
    }
    
}
