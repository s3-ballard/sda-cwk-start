/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "FilePlayerGui.h"
#include "XYGrid.hpp"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
public Slider::Listener, public Thread
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    
    void run() override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    void sliderValueChanged (Slider* slider) override;
    
    int sampRate;
    
private:
    static const int NumFilePlayers = 1;
    Audio& audio;
    ScopedPointer<FilePlayerGui> filePlayerGui[NumFilePlayers];
    //FilePlayerGui filePlayerGui;
    
    Slider gain;
    Slider tempoSlider;
    
   XYGrid grid;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
