/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : Thread ("CounterThread"), audio (audio_)
{
    
    startThread();
    
    //Add the filePlayers to the GUI - may be able to combine into one
    for (int i = 0; i < NumFilePlayers; i++){
        filePlayerGui[i] = new FilePlayerGui(audio_.getFilePlayer(i));
    }
    for (int counter = 0; counter < NumFilePlayers; counter++){
        addAndMakeVisible(filePlayerGui[counter]);
    }
    
    setSize (500, 900);
    //Overall volume slider - !! need to work out seperate slider for each fileplayer
    addAndMakeVisible(gain);
    gain.addListener(this);
    gain.setRange(0, 1);
    //setting global Tempo control
    addAndMakeVisible(tempoSlider);
    tempoSlider.addListener(this);
    tempoSlider.setRange(20, 180);
    tempoSlider.setValue(120);
    //sample rate send to the fileplayer for tempo equation
    sampRate = audio.sampleRate.get();
    
    //  grid.boundarys(getWidth()-20, getHeight() * 05);
}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::run()
{
    while (!threadShouldExit())
    {
        //assign the coordinate amplitude values to audio.
        audio.xyAmpTL = grid.gridAmpTL();
        audio.xyAmpTR = grid.gridAmpTR();
        audio.xyAmpBL = grid.gridAmpBL();
        audio.xyAmpBR = grid.gridAmpBR();
    }
    
}

void MainComponent::resized()
{
    int height = 0;
    //change size of each fileplayer - NEED TO EDIT THIS
    for (int counter = 0; counter < NumFilePlayers; counter++){
        filePlayerGui[counter]->setBounds (0, 50, getWidth(), 650);
        height += 200;
    }
    //newer stuff
    gain.setBounds(0, 650, getWidth(), 50);
    tempoSlider.setBounds(0, 700, getWidth(), 50);
    //grid.setBounds(10,50,getWidth()-20, getHeight * 0.5);
    //grid.boundarys(getWidth()-20, getHeight()*0.5); //update the max bounday information of the grid when resized
    
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    //Main Volume
    if (slider == &gain)
    audio.gain = gain.getValue();
    
    //send the tempo and the sample rate to convert audio file into the right time
    if (slider == &tempoSlider)
        filePlayerGui[0]->tempoSync(tempoSlider.getValue(), sampRate);
}



