//
//  XYGrid.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 07/12/2016.
//
//

#include "XYGrid.hpp"

XYGrid::XYGrid()
{
    
}

XYGrid::~XYGrid()
{
    
}

void XYGrid::paint(Graphics& g)
{
    if (isMouseIn == true)
        g.setColour(Colours::aliceblue);
    
    else if (isMouseIn == false)
        g.setColour(Colours::whitesmoke);
    
    if (isMouseDown == true && isMouseIn == true)
        g.setColour(Colours::magenta);
    
    g.drawRect(getLocalBounds());
    g.fillRect(getLocalBounds());
    
    g.setColour(Colours::black);
    g.drawEllipse(xCursor, yCursor, getWidth() * 0.1, getHeight() * 0.1, 1); //need to alter to get precise cursor placement (the getWidth and getHeight parts)
    g.fillEllipse(xCursor, yCursor, getWidth() * 0.1, getHeight() * 0.1);
    
}

void XYGrid::mouseEnter(const MouseEvent& event)
{
    isMouseIn = true;
    repaint();
}

void XYGrid::mouseDown(const MouseEvent& event)
{
    isMouseDown = true;
    repaint();
}

void XYGrid::mouseUp(const MouseEvent &event)
{
    isMouseDown = false;
    repaint();
}

void XYGrid::mouseExit(const MouseEvent &event)
{
    isMouseIn = false;
    repaint();
}

void XYGrid::mouseDrag(const MouseEvent &event)
{
    x = event.x;
    y = event.y;
    
    if (event.x > maxX)
        x = maxX;
    if (event.x < 0)
        x = 0;
    if (event.y > maxY)
        y = maxY;
    if (event.y < 0)
        y = 0;
    //may be able to use already exisiting variable
    xCursor = x;
    yCursor = y;
    
    if (isMouseIn == true){
        x = x/maxX;
        y = y/maxY;
    }
    //need to send the x and y out as an amplitude value, probably make atomic and send to audio
}

void XYGrid::boundarys(float x, float y)
{
    //get the maximum boundarys
    maxX = x;
    maxY = y;
}

float XYGrid::gridAmpTL()
{
    //need to adjust for size
    amp = ((1.f-x) + (1.f - y)) * 0.5;
    //stop any extranouous amp values.
    if (amp > 1)
        amp = 1.0;
    if (amp < 0)
        amp = 0;
    
    return amp;
    
}

float XYGrid::gridAmpTR()
{
    amp = (x + (1.f-y)) * 0.5;
    if (amp > 1)
        amp = 1.0;
    if (amp < 0)
        amp = 0;
    
    return amp;
}

float XYGrid::gridAmpBL()
{
    amp = ((1.f - x) + y) * 0.5;
    if (amp > 1)
        amp = 1.0;
    if (amp < 0)
        amp = 0;
    
    return amp;
}

float XYGrid::gridAmpBR()
{
    amp = (x + y) * 0.5;
    if (amp > 1)
        amp = 1.0;
    if (amp < 0)
        amp = 0;
    
    return amp;
}
