//
//  USG.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 08/12/2016.
//
//

#include "USG.hpp"
#include <cmath>

USG::USG()
{
    reset();
}

USG::~USG()
{
    
}

void USG::setFrequency(float freq)
{
    frequency = freq;
    phaseInc = (M_PI * frequency) /sampleRate;
}

void USG::setAmount(float amnt)
{
    amount = amnt;
}

void USG::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency(5);
    setAmount(0.f);
}

void USG::setSampleRate(float sR)
{
    sampleRate = sR;
    setFrequency(frequency);
}

float USG::nextSample()
{
    float out;
    if (trigger == true)
    {
        out = renderShape(phase) * amount;
        phase = phase + phaseInc;
        if (phase > 1.f * M_PI)
        {
            phase -= (1.f * M_PI);
            
            if (cycleOn == false)
            {
                trigger = false;
            }
            else if (cycleOn == true)
            {
                trigger = true;
            }
            
            
        }
    }
    
    if (trigger == false)
    {
        out = 0;
    }
    return out;
}

float USG::renderShape(const float currentPhase)
{
    float out;
    
    if (currentPhase < M_PI) {
        out = 0.5 + (M_PI * currentPhase);
    }
    
    else {
        out = 1.5 - (M_PI * currentPhase);
    }
    
    return out;
}
