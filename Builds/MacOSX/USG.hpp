//
//  USG.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 08/12/2016.
//
//

#ifndef USG_hpp
#define USG_hpp

#include <stdio.h>

class USG
{
public:
    USG();
    
    ~USG();
    
    void setFrequency (float freq);
    
    void setAmount (float amnt);
    
    void reset();
    
    void setSampleRate(float sR);
    
    float nextSample();
    
    float renderShape (const float currentPhase);
    
    bool cycleOn;
    bool trigger;
    
private:
    float frequency;
    float amount;
    float sampleRate;
    float phase;
    float phaseInc;
    
    
    
};

#endif /* USG_hpp */
