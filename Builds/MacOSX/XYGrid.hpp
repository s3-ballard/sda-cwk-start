//
//  XYGrid.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 07/12/2016.
//
//

#ifndef XYGrid_hpp
#define XYGrid_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class XYGrid : public Component

{
public:
    XYGrid();
    ~XYGrid();
    
    void paint (Graphics& g) override;
    void mouseEnter (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    void mouseDrag (const MouseEvent& event) override;
    void boundarys(float x, float y);
    
    float gridAmpTL();
    float gridAmpTR();
    float gridAmpBL();
    float gridAmpBR();
    
    float x;
    float y;
    //may be able to make private
    float maxX;
    float maxY;
    
private:
    bool isMouseIn;
    bool isMouseDown;
    float amp;
    //used for visual feedback of grid location
    float xCursor;
    float yCursor;
};

#endif /* XYGrid_hpp */
